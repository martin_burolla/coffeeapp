## Coffee App [![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=56d0fcd50a10690100b3d62a&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/56d0fcd50a10690100b3d62a/build/latest)
A universal app written in Objective-C for iOS that displays the coffee shops near you using the [Foursquare](https://foursquare.com/dev/) API.

<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/CoffeeApp1.png width=200 length=400 >
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/MoreDetails.png width=200 length=400>
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/starbucksfail.png width=200 length=400>
</p>

<p align="center">
<b>Coffee Shops with Ratings</b>
</p>

## Purpose
The purpose of this app is not to create the world's best coffee app with a stellar visual design and wide feature set, but to address the main problem that continues to plague the software industry today: Architecture. 

Software solutions can very quickly approach end of life when there is no architecture, the architecture is not understood, or the architecture is not enforced. Poorly architectured software can make the easiest task feel like an epic battle.  This project illustrates a software architecture that I created called **Questions**.  The Questions architecture style provides a model that is easy to understand, easy to implement and easy to extend.  Questions makes it easy to answer the common questions that software developers face in their apps.

## Questions

There are two main pillars in the Questions architecture:

* Layers, layers, layers
* Keep ViewControllers <em>light</em>

#### Layers

Many iOS applications contain these three components:

* User Interface
* Application Logic
* Data

The architecture used in this application uses these logical separations and can be viewed in this diagram.

<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/GeneralArch.png >
</p>

<p align="center">
<b>Questions Architecture Style for Coffee App</b>
</p>


A unique feature of Xcode is that it allows folders (groups) to be dragged in any order in the project.  We can take advantage of this capability and create a virtual layer diagram right in our project.  


<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/QuestionsArch.png >
</p>

<p align="center">
<b>CoffeeApp Architecture</b>
</p>

This diagram answers the following questions:

* Where is <em>all</em> the data that this app uses?
* Where is <em>all</em> the sensitive data?
* Where is <em>all</em> the UI?
* Where is <em>all</em> the application logic?

This gives developers the confidence and guidence they need to quickly contribute to the project. It also provides a way to manage software development work by assigning developers to specific parts (layers) of the app.

##### From the Bottom Up 

The Data Access Layer (DAL) is the bottom layer in the Questions architecture stack.  The idea is to clearly identify all of the data sources that your app uses.  These sources can be external or internal data sources.   

Example external data sources:

* Foursquare
* Facebook
* Apple CloudKit
* Amazon (AWS)

Example internal data sources:

* CoreData
* NSUserDefaults
* Local cache
* Plist

A data proxy acts as a boundary class that performs one only job — to import and export data from a data source in the cleanest possible manner. One data proxy is assigned to each data source. Data proxies do not have other references to other data proxies. If a call to a datasource become long and blocks the UI, NSOperation can be used in a data proxy to perform work on a queue other than the main UI queue.  

When implementing a data proxy, a protocol may be used to further abstract the implementation from the specification.  This provides a way to swap out the implementation of the proxy for testing or development.

It’s important to treat the internal data sources as if they were external data sources.  This makes understanding <em>all</em> of the data for your app very easy, and answers a very important question: Where is <em>all</em> the data for this app coming from?

<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/DAL.png >
</p>
<p align="center">
<b>Internal and External Data Sources</b>
</p>

Having a solid understanding of the data for app will assist your troubleshooting efforts.  If the data is not understood, the application logic that uses the data will not be understood.  Data is the foundation for your app and Questions makes the data very easy to see for technical and non-technical people.

#####Manager Layer

The manager layer is the middle layer in the Questions stack and answers the question: What exactly does this app do?  The managers contain references to data proxies.  Mangers use data proxies to help them do work.  Managers can have references to other managers.  To keep code consistent, managers return delegate callbacks to indicate a completion of a task.  This answers the following question: How do I know when something is finished?

#####Security

There’s a thin security layer in the coffee app.  Sensitive items such as API keys, keychain access and validation code are located in this layer.  Placing all the sensitive items for your app in one place makes it very easy to perform a security audit and to redact data if necessary.   It’s helpful to have all the validation logic for the entire app in one place. This makes the security folks happy and answers the question: How is the app validating <em>all</em> the data?

#### View Controllers

<em>Joke of the day: What does MVC stand for? Massive View Controller!</em>

The coding style used in ViewControllers is the second pillar to Questions. It's very easy to add business logic and other items that don't belong in a ViewController.  Without proper guideance, ViewControllers can become massive and hard to maintain.  Due digilance by developers is needed to prevent putting things in ViewControllers that don't belong.

Questions attempts to clean this clutter with the use of Managers and delegate callbacks.  The Questions architecture dictates that ViewControllers will contain references to Manager objects.  When a manager is done performing a task, a delegate callback is used to indicate completion.  Managers should not return completion blocks to a ViewController.  This approach aligns well with Apple's framework.

<p align="center">
<img src=https://github.com/mburolla/ReadMePix/blob/master/coffeeapp/CoffeeAppVC.png >
</p>

<p align="center">
<b>View Controllers Contain References to Managers</b>
</p>

## Summary

The key principle with Questions is to strictly obey these two words: **All** and **One**.  If you find yourself saying, "All the data is here, except for this..." then you have violated the Questions architecture.  Questions is easy to understand, easy to use and allows developers to stay creative in their software development process.  Happy developers are developers that will stay with your company.

## Miscellaneous

* This project was built with CocoaPods, use CoffeeApp.xcworkspace to open the project
* Builds with Xcode 7.2.1 Master: [![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=56d0fcd50a10690100b3d62a&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/56d0fcd50a10690100b3d62a/build/latest)
