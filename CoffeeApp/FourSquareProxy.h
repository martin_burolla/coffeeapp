//
//  FourSquareProxy.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/13/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Foursquare2.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CoffeeHousePointAnnotationViewModel.h"
#import "CoffeeHouseMoreInfoViewModel.h"


typedef void (^coffeeHousesNearLocationBlock)(NSArray <CoffeeHousePointAnnotationViewModel *>*coffeeHouses);
typedef void (^venueGetDetailBlock)(CoffeeHouseMoreInfoViewModel *coffeeHouseDetail);

@interface FourSquareProxy : NSObject

-(void)coffeeHousesNearLocation:(CLLocation *)location withCompletion:(coffeeHousesNearLocationBlock)callbackBlock;
-(void)venueGetDetail:(NSString *)fourSquareVenueID withCompletion:(venueGetDetailBlock)callbackBlock;

@end