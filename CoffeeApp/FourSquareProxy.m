//
//  FourSquareProxy.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/13/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "FourSquareProxy.h"

static const int kFourSquareVenueSearchRadius = 500;
static NSString* kFourSquareCoffeeShopCategoryID = @"4bf58dd8d48988d1e0931735"; // All categories: https://developer.foursquare.com/categorytree

@implementation FourSquareProxy


#pragma mark - Getters

- (void)coffeeHousesNearLocation:(CLLocation *)location withCompletion:(coffeeHousesNearLocationBlock)callbackBlock {
    [Foursquare2 venueSearchNearByLatitude:@(location.coordinate.latitude)
                                 longitude:@(location.coordinate.longitude)
                                     query:nil
                                     limit:nil
                                    intent:intentCheckin
                                    radius:@(kFourSquareVenueSearchRadius)
                                categoryId:kFourSquareCoffeeShopCategoryID
                                  callback:^(BOOL success, id result) {
                                      if (success) {
                                          callbackBlock([self buildPointAnnotationList: result]);
                                      } else {
                                          callbackBlock(nil);
                                      }
    }];
}

// https://developer.foursquare.com/docs/responses/venue
- (void)venueGetDetail:(NSString *)fourSquareVenueID withCompletion:(venueGetDetailBlock)callbackBlock {
    [Foursquare2 venueGetDetail:fourSquareVenueID
                       callback:^(BOOL success, id result) {
                           if (success) {
                               callbackBlock([self buildMoreDetailsViewModel: result]);
                           } else {
                               callbackBlock(nil);
                           }
    }];
}


#pragma mark - Helpers

- (NSArray <CoffeeHousePointAnnotationViewModel *> *)buildPointAnnotationList:(id)result {
    NSMutableArray *pointAnnotationList  = [[NSMutableArray alloc]init];
    NSArray *coffeeHouses = [result valueForKeyPath:@"response.venues"];
    for (NSDictionary *coffeeHouse in coffeeHouses) {
        CoffeeHousePointAnnotationViewModel *point = [[CoffeeHousePointAnnotationViewModel alloc] init];
        point.fourSquareVenueID = [coffeeHouse valueForKeyPath:@"id"];
        point.title = [coffeeHouse valueForKey:@"name"];
        point.subtitle = [coffeeHouse valueForKeyPath:@"contact.formattedPhone"];
        point.coordinate  = CLLocationCoordinate2DMake([[coffeeHouse valueForKeyPath:@"location.lat"] doubleValue],
                                                       [[coffeeHouse valueForKeyPath:@"location.lng"] doubleValue]);
        [pointAnnotationList addObject: point];
    }
    return  [[NSArray alloc]initWithArray: pointAnnotationList];
}

- (CoffeeHouseMoreInfoViewModel*)buildMoreDetailsViewModel:(id)result {
    CoffeeHouseMoreInfoViewModel *moreInfo = [[CoffeeHouseMoreInfoViewModel alloc]init];
    NSArray *moreDetails = [result valueForKeyPath:@"response.venue"];
    moreInfo.isOpen = [[moreDetails valueForKeyPath: @"hours.isOpen"] boolValue];
    moreInfo.status = [moreDetails valueForKeyPath: @"hours.status"];
    moreInfo.rating = [[moreDetails valueForKeyPath: @"rating"] floatValue];
    moreInfo.hereNowSummary = [moreDetails valueForKeyPath: @"hereNow.summary"];
    moreInfo.coffeeHouseName = [moreDetails valueForKeyPath: @"name"];
    return moreInfo;
}

@end