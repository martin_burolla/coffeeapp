//
//  AlertControllerFactory.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/13/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface AlertControllerFactory : NSObject

+ (UIViewController *)createErrorMessageViewController:(NSString*)message;

@end
