//
//  RatingView.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/17/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingView : UIView

@property (nonatomic, strong) UILabel *ratingLabel;

@end