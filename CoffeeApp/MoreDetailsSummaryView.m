//
//  MoreDetailsSummaryView.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/20/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "Masonry.h"
#import "MoreDetailsSummaryView.h"


@interface MoreDetailsSummaryView()

@property (nonatomic, strong) UILabel *coffeeHouseLabel;
@property (nonatomic, strong) UILabel *hereNowSummaryLabel;
@property (nonatomic, strong) UILabel *status;
@property (nonatomic, assign) float isOpen;

@end

@implementation MoreDetailsSummaryView


#pragma mark - Lifecycle

-(instancetype) init {
    self = [super init];
    if (self!=nil) {
        [self setupUI];
        [self setupConstraints];
    }
    return self;
}


#pragma mark - Databinding

-(void)drawRect:(CGRect)rect { // Also called by setNeedsDisplay.
    self.coffeeHouseLabel.text = self.coffeeHouseMoreInfoViewModel.coffeeHouseName;
    self.hereNowSummaryLabel.text = self.coffeeHouseMoreInfoViewModel.hereNowSummary;
    self.status.text = self.coffeeHouseMoreInfoViewModel.status;
    self.isOpen = self.coffeeHouseMoreInfoViewModel.isOpen;
    
    if (self.isOpen) {
        self.status.backgroundColor = [UIColor greenColor];
    } else {
        self.status.backgroundColor = [UIColor redColor];
    }
}


#pragma mark - Private UI Construction

- (void)setupUI {
    self.coffeeHouseLabel = [[UILabel alloc]init];
    self.coffeeHouseLabel.backgroundColor = [UIColor whiteColor];
    [self addSubview: self.coffeeHouseLabel];
    
    self.hereNowSummaryLabel = [[UILabel alloc]init];
    self.hereNowSummaryLabel.backgroundColor = [UIColor whiteColor];
    [self addSubview: self.hereNowSummaryLabel];
    
    self.status = [[UILabel alloc]init];
    [self addSubview: self.status];
}


#pragma mark - Private UI Constraints

- (void)setupConstraints {
    self.coffeeHouseLabel.mas_key = @"Coffee House label";
    [self.coffeeHouseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.height.equalTo(@50);
        make.leading.equalTo(self.mas_leading).with.offset(0);
        make.trailing.equalTo(self.mas_trailing).with.offset(0);
    }];
    
    self.hereNowSummaryLabel.mas_key = @"Here now summary";
    [self.hereNowSummaryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.coffeeHouseLabel.mas_bottom);
        make.height.equalTo(@50);
        make.leading.equalTo(self.mas_leading).with.offset(0);
        make.trailing.equalTo(self.mas_trailing).with.offset(0);
    }];
    
    self.status.mas_key = @"Status";
    [self.status mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hereNowSummaryLabel.mas_bottom);
        make.height.equalTo(@50);
        make.leading.equalTo(self.mas_leading).with.offset(0);
        make.trailing.equalTo(self.mas_trailing).with.offset(0);
    }];
}

@end