//
//  RatingView.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/17/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "Masonry.h"
#import "RatingView.h"


const int kFontSizeOfRatingLabel = 120;
const int kVerticalOffestOfRatingBar = 150;
const int kPointsPerRating = 25;

@interface RatingView()

@property (nonatomic, strong) UIView *ratingBar;

@end

@implementation RatingView


#pragma mark - Lifecycle

-(instancetype) init {
    self = [super init];
    if (self!=nil) {
        [self setupUI];
        [self setupConstraints];
    }
    return self;
}


#pragma mark - Private UI Construction

- (void)setupUI {
    
    self.backgroundColor = [UIColor whiteColor];
    
    // Top label.
    self.ratingLabel = [[UILabel alloc]init];
    self.ratingLabel.backgroundColor = [UIColor whiteColor];
    self.ratingLabel.font = [UIFont fontWithName:@"Helvetica" size:kFontSizeOfRatingLabel];
    [self addSubview:self.ratingLabel];
    
    // Rating bar.
    self.ratingBar = [[UIView alloc]init];
    [self addSubview: self.ratingBar];
}


-(void)drawRect:(CGRect)rect {
    float rating = [self.ratingLabel.text floatValue];
    float maxHeightOfBar = self.bounds.origin.y + 150 ;
    float adjustment = (10.0 - rating) * kPointsPerRating;  // 250 = max height, 10 star rating.
    
    
    if (rating > 8.0) {
        self.ratingBar.backgroundColor = [UIColor greenColor];
    } else {
        self.ratingBar.backgroundColor = [UIColor redColor];
    }
    
    // The initial starting size/position of the bar.
    self.ratingBar.frame = CGRectMake(self.bounds.origin.x + 50,
                                      self.bounds.size.height,
                                      self.bounds.size.width - 100,
                                      self.bounds.size.height - kVerticalOffestOfRatingBar);
    [UIView animateWithDuration: 1
                          delay: 0
                        options: UIViewAnimationCurveLinear | UIViewAnimationCurveLinear
                     animations:^{
                         [self layoutIfNeeded];
                         
                         // The final size/position of the bar.
                         self.ratingBar.frame = CGRectMake(self.bounds.origin.x + 50,
                                                           maxHeightOfBar + adjustment,
                                                           self.bounds.size.width - 100,
                                                           self.bounds.size.height - kVerticalOffestOfRatingBar);
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}


#pragma mark - Private UI Constraints

- (void)setupConstraints {
    
    // Top label.
    self.ratingLabel.mas_key = @"Rating Label";
    [self.ratingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).with.offset(10);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@(100));
    }];
    
    // I would love to animate the constraint, but for whatever reason, this version
    // of the iOS base SDK and Masonry don't really work correctly?
    
//    Rating bar.
//    self.ratingBar.mas_key = @"Rating bar";
//    [self.ratingBar mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(@(200));
//        make.leading.equalTo(self.mas_leading).with.offset(80);
//        make.trailing.equalTo(self.mas_trailing).with.offset(-80);
//        make.bottom.equalTo(self.mas_bottom);
//    }];
}

@end