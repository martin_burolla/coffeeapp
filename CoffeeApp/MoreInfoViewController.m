//
//  MoreInfoViewController.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "Masonry.h"
#import "RatingView.h"
#import "FourSquareManager.h"
#import "MoreInfoViewController.h"
#import "MoreDetailsSummaryView.h"


static const int kDismissButtonHeight = 75;

@interface MoreInfoViewController () <FourSquareManagerDelegate>

@property (nonatomic, strong) MoreDetailsSummaryView *moreDetailsView; // Top part of view
@property (nonatomic, strong) RatingView *ratingView;                  // Bottom part of view
@property (nonatomic, strong) UIButton *dismissButton;                 // Bottom button
@property (nonatomic, strong) FourSquareManager *fourSquareManager;    // App logic

@end

@implementation MoreInfoViewController


#pragma mark - Life Cycle

- (instancetype) init {    
    self = [super init];
    if (self!=nil) {
        _fourSquareVenueID = [[NSString alloc]init];
        _fourSquareManager = [[FourSquareManager alloc]init]; 
        _fourSquareManager.delegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupConstraints];
    [self.fourSquareManager venueGetDetailsForFourSquareID: self.fourSquareVenueID];
}


#pragma mark - Callback FourSquareManagerDelegate

- (void)FourSquareManager:(id<FourSquareManagerProtocol>)FourSquareManager didFinishGettingVenueDetails:(NSError *)error {
    self.moreDetailsView.coffeeHouseMoreInfoViewModel = self.fourSquareManager.venueDetails;
    self.ratingView.ratingLabel.text = self.fourSquareManager.venueDetails.formattedRating;
    [self.moreDetailsView setNeedsDisplay];
    [self.ratingView setNeedsDisplay];
}


#pragma mark - User Interaction

- (void)dismissButtonTouchUpInside:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private UI Construction

- (void)setupUI {
    
    // Background.
    [self createBlurBackground];
    
    // The text labels at the top.
    self.moreDetailsView = [[MoreDetailsSummaryView alloc] init];
    [self.view addSubview:self.moreDetailsView];
    
     // The rating bar at the bottom.
    self.ratingView = [[RatingView alloc]init];
    [self.view addSubview:self.ratingView];
    
    // Dismiss button at very bottom.
    _dismissButton = [[UIButton alloc]init];
    _dismissButton.backgroundColor = [UIColor darkGrayColor];
    [_dismissButton setTitle: @"OK" forState:UIControlStateNormal];
    [_dismissButton addTarget:(self) action: @selector(dismissButtonTouchUpInside:) forControlEvents: UIControlEventTouchUpInside];
    [self.view addSubview: _dismissButton];
}

- (void)createBlurBackground {
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *beView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    beView.frame = self.view.bounds;
    
    self.view.frame = self.view.bounds;
    self.view.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:beView atIndex:0];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
}


#pragma mark - Private UI Constraints

- (void)setupConstraints {
    
    // The text labels at the top.
    self.moreDetailsView.mas_key = @"More details summary view";
    [self.moreDetailsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(40);
        make.height.equalTo(@150);
        make.leading.equalTo(self.view.mas_leading).with.offset(60);
        make.trailing.equalTo(self.view.mas_trailing).with.offset(-60);
    }];
    
    // The rating bar at the bottom.
    self.ratingView.mas_key = @"Rating View";
    [self.ratingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(400));
        make.leading.equalTo(self.view.mas_leading).with.offset(60);
        make.trailing.equalTo(self.view.mas_trailing).with.offset(-60);
        make.bottom.equalTo(self.dismissButton.mas_top);
    }];
    
    // Dismiss button at very bottom.
    self.dismissButton.mas_key = @"Dismiss Button";
    [self.dismissButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(kDismissButtonHeight));
        make.leading.equalTo(self.view.mas_leading);
        make.trailing.equalTo(self.view.mas_trailing);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}

@end