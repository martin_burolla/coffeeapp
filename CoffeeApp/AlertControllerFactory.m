//
//  AlertControllerFactory.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/13/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "AlertControllerFactory.h"


@implementation AlertControllerFactory

+ (UIViewController *)createErrorMessageViewController:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"CoffeeApp"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    return alert;
}

@end
