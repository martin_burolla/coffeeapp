//
//  AppDelegate.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/12/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end