//
//  TempViewController.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/16/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "TempViewController.h"

/** 
 Used for stubbing out new UI stuff that is located too deep within the app.
 */

@interface TempViewController ()

@end

@implementation TempViewController


#pragma mark - Lifecycle



#pragma mark - Private UI Construction



#pragma mark - Private UI Constraints


@end