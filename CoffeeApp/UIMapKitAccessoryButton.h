//
//  UIMapKitAccessoryButton.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CoffeeHousePointAnnotationViewModel.h"

@interface UIMapKitAccessoryButton : UIButton

@property(nonatomic, strong) CoffeeHousePointAnnotationViewModel *pointAnnotation;

@end