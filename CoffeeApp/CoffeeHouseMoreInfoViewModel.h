//
//  CoffeeHouseMoreInfoViewModel.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIMapKitAccessoryButton.h"

@interface CoffeeHouseMoreInfoViewModel : NSObject

@property(nonatomic, assign) BOOL isOpen;
@property(nonatomic, assign) double rating;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, strong) NSString *coffeeHouseName;
@property(nonatomic, strong) NSString *hereNowSummary;

-(NSString*)formattedRating;

@end