//
//  ViewController.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/12/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//


#import "Masonry.h"
#import "Foursquare2.h"
#import "HomeViewController.h"
#import "FourSquareManager.h"
#import "AlertControllerFactory.h"
#import "MoreInfoViewController.h"
#import "UIMapKitAccessoryButton.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface HomeViewController () <MKMapViewDelegate,
                              CLLocationManagerDelegate,
                              FourSquareManagerDelegate>

@property (nonatomic, strong) MKMapView *mapView;                    // Map
@property (nonatomic, strong) CLLocationManager *locationManager;    // App logic
@property (nonatomic, strong) FourSquareManager *fourSquareManager;  // App logic

@end

@implementation HomeViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLocationManager];
    [self setupFourSquareManager];
    [self setupUI];
    [self setupConstraints];
}


#pragma mark - Callback CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [self setupMapForLocation:newLocation];
    [self.fourSquareManager coffeeHousesNearLocation: newLocation];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%@", error.description);
}


#pragma mark - Callback FourSquareManagerDelegate

- (void)FourSquareManager:(id<FourSquareManagerProtocol>)FourSquareManager didFinishGettingCoffeeHouses:(NSError *)error {
    if (error == nil) {
         [self.mapView addAnnotations:self.fourSquareManager.coffeeHouses];
    } else {
        [self presentViewController:[AlertControllerFactory createErrorMessageViewController:error.domain] animated:YES completion:nil];
    }
}


#pragma mark - Callback MKMapViewDelegate

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    annotationView.canShowCallout = YES;
    UIMapKitAccessoryButton *moreInfoButton = [UIMapKitAccessoryButton buttonWithType:UIButtonTypeDetailDisclosure];
    moreInfoButton.pointAnnotation = annotation;
    annotationView.rightCalloutAccessoryView =moreInfoButton;
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    MoreInfoViewController *moreInfoViewController = [[MoreInfoViewController alloc]init];
    moreInfoViewController.fourSquareVenueID = ((UIMapKitAccessoryButton*)control).pointAnnotation.fourSquareVenueID;
    moreInfoViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:moreInfoViewController animated:YES completion:nil];
}


#pragma mark - User Interaction

- (void)moreInfoButtonTouchUpInside:(id)sender {
    MoreInfoViewController *moreInfoViewController = [[MoreInfoViewController alloc]init];
    [self presentViewController: moreInfoViewController animated:YES completion:nil];
}


#pragma mark - Private UI Construction

- (void)setupUI {
    _mapView = [[MKMapView alloc]init];
    _mapView.delegate = self;
    [self.view addSubview:_mapView];
}

- (void)setupConstraints {
    self.mapView.mas_key = @"MapView";
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.leading.equalTo(self.view.mas_leading);
        make.trailing.equalTo(self.view.mas_trailing);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}


#pragma mark - Private SubObject Construction

- (void)setupMapForLocation:(CLLocation *)newLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.003;
    span.longitudeDelta = 0.003;
    CLLocationCoordinate2D location;
    location.latitude = newLocation.coordinate.latitude;
    location.longitude = newLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [self.mapView setRegion:region animated:YES];
}

-(void)setupLocationManager {
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined ) {
            [self.locationManager requestWhenInUseAuthorization];
        } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                   [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
            [self.locationManager startUpdatingLocation];
        }
    } else {
        [self.locationManager startUpdatingLocation];
    }
}

-(void)setupFourSquareManager {
    self.fourSquareManager = [[FourSquareManager alloc]init];
    self.fourSquareManager.delegate = self;
}

@end