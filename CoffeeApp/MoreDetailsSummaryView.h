//
//  MoreDetailsSummaryView.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/20/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoffeeHouseMoreInfoViewModel.h"


@interface MoreDetailsSummaryView : UIView

@property (nonatomic, strong) CoffeeHouseMoreInfoViewModel *coffeeHouseMoreInfoViewModel;

@end