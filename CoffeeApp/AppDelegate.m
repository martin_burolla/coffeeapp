//
//  AppDelegate.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/12/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "AppDelegate.h"
#import "Foursquare2.h"
#import "Keys.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [Foursquare2 handleURL:url];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Foursquare2 setupFoursquareWithClientId:fourSquareClientID
                                      secret:fourSquareSecret
                                 callbackURL:fourSquareCallbackURL];
    
    return YES;
}

@end