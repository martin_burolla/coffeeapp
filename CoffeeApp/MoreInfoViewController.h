//
//  MoreInfoViewController.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoffeeHouseMoreInfoViewModel.h"


@interface MoreInfoViewController : UIViewController

@property (nonatomic, strong) NSString *fourSquareVenueID;

@end