//
//  CoffeeHousePointAnnotationViewModel.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/15/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CoffeeHousePointAnnotationViewModel : MKPointAnnotation

@property (nonatomic, strong) NSString *fourSquareVenueID;

@end