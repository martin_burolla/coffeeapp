//
//  FourSquareManager.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FourSquareProxy.h"
#import "CoffeeHousePointAnnotationViewModel.h"
#import "CoffeeHouseMoreInfoViewModel.h"

@protocol FourSquareManagerDelegate;

@protocol FourSquareManagerProtocol <NSObject>
@property (nonatomic, weak) id<FourSquareManagerDelegate> delegate;
@end

@protocol FourSquareManagerDelegate <NSObject>
@optional
- (void)FourSquareManager:(id<FourSquareManagerProtocol>)FourSquareManager didFinishGettingCoffeeHouses:(NSError *)error;
- (void)FourSquareManager:(id<FourSquareManagerProtocol>)FourSquareManager didFinishGettingVenueDetails:(NSError *)error;
@end

@interface FourSquareManager : NSObject <FourSquareManagerProtocol>
@property (nonatomic, strong) NSArray <MKPointAnnotation *>*coffeeHouses;
@property (nonatomic, strong) CoffeeHouseMoreInfoViewModel *venueDetails;

-(void)coffeeHousesNearLocation:(CLLocation *)location;
-(void)venueGetDetailsForFourSquareID:(NSString *)fourSquareID;
@end