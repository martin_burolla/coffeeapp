//
//  CoffeeHouseMoreInfoViewModel.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "CoffeeHouseMoreInfoViewModel.h"

@implementation CoffeeHouseMoreInfoViewModel


#pragma mark - Presentation

-(NSString*)formattedRating {
    return [NSString stringWithFormat: @"%2.1f", self.rating];
}

@end