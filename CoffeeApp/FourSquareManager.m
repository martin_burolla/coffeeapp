//
//  FourSquareManager.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "FourSquareManager.h"

@interface FourSquareManager()
@property (nonatomic, strong) FourSquareProxy *fourSquareProxy;
@end

@implementation FourSquareManager

@synthesize delegate;


# pragma mark - Construction

-(instancetype) init {
    self = [super init];
    if (self!=nil) {
        _fourSquareProxy = [[FourSquareProxy alloc]init]; // Possibly inject this dependency somewhere upstream.
    }
    return self;
}


# pragma mark - Getters

-(void)coffeeHousesNearLocation:(CLLocation *)location {
        [self.fourSquareProxy coffeeHousesNearLocation:location withCompletion:^(NSArray <CoffeeHousePointAnnotationViewModel *>*coffeeHouses) {
            self.coffeeHouses = coffeeHouses;
             if ([self.delegate respondsToSelector: @selector (FourSquareManager:didFinishGettingCoffeeHouses:)]) {
                 NSError *error = nil;
                 if (coffeeHouses == nil) {
                    error = [[NSError alloc] initWithDomain: @"Error: FourSquare API." code:500 userInfo:nil];
                 }
                 [self.delegate FourSquareManager:self didFinishGettingCoffeeHouses:error];
             }
        }];
}

-(void)venueGetDetailsForFourSquareID:(NSString *)fourSquareID {
    [self.fourSquareProxy venueGetDetail:fourSquareID withCompletion:^(CoffeeHouseMoreInfoViewModel *coffeeHouseDetail) {
        self.venueDetails = coffeeHouseDetail;
        if ([self.delegate respondsToSelector: @selector (FourSquareManager:didFinishGettingVenueDetails:)]) {
            NSError *error = nil;
            if (coffeeHouseDetail == nil) {
                error = [[NSError alloc] initWithDomain: @"Error: FourSquare API." code:500 userInfo:nil];
            }
            [self.delegate FourSquareManager:self didFinishGettingVenueDetails:error];
        }
    }];
}

@end