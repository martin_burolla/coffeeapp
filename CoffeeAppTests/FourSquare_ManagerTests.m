//
//  CoffeeAppTests.m
//  CoffeeAppTests
//
//  Created by Marty Burolla on 11/12/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FourSquareManager.h"
#import "UnitTestHelper.h"


@interface FourSquare_ManagerTests : XCTestCase <FourSquareManagerDelegate>

@property(nonatomic, strong) XCTestExpectation *expectation;

@end

@implementation FourSquare_ManagerTests

- (void)setUp {
    [super setUp];
    _expectation =  [self expectationWithDescription:@"FourSquare API should return proper data."];
}

- (void)tearDown {
    [super tearDown];
}

- (void)test_GetCoffeeHouses {

    // Arrange.
    CLLocation *location =  [UnitTestHelper createLocationForDowntownAustin];
    FourSquareManager *fourSquareManager = [[FourSquareManager alloc]init];
    fourSquareManager.delegate = self;
    
    // Act.
    [fourSquareManager coffeeHousesNearLocation: location];
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        
        // Assert.
        XCTAssertTrue(fourSquareManager.coffeeHouses.count > 0, @"No coffee houses in austin?");
        XCTAssertTrue(error == nil, @"Error should be nil.");
    }];
}

- (void)FourSquareManager:(id<FourSquareManagerProtocol>)FourSquareManager didFinishGettingCoffeeHouses:(NSError *)error {
     [self.expectation fulfill];
}

- (void)test_GetVenueDetails {
    
    // Arrange.
    FourSquareManager *fourSquareManager = [[FourSquareManager alloc]init];
    fourSquareManager.delegate = self;
    
    // Act.
    [fourSquareManager venueGetDetailsForFourSquareID: @"52869068498e3289da67410f"]; // Starbucks on Congress.
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        
        // Assert.
        XCTAssertTrue(fourSquareManager.venueDetails != nil, @"No details available for StarBucks on Congress?");
    }];
}

- (void)FourSquareManager:(id<FourSquareManagerProtocol>)FourSquareManager didFinishGettingVenueDetails:(NSError *)error {
    [self.expectation fulfill];
}



@end