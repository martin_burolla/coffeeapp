//
//  UnitTestHelper.h
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface UnitTestHelper : NSObject

+ (CLLocation*)createLocationForDowntownAustin;

@end
