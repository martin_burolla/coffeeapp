//
//  UnitTestHelper.m
//  CoffeeApp
//
//  Created by Marty Burolla on 11/14/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "UnitTestHelper.h"

@implementation UnitTestHelper

+ (CLLocation*)createLocationForDowntownAustin {
    NSNumber *lat = [[NSNumber alloc] initWithDouble:30.26715300];
    NSNumber *lon = [[NSNumber alloc] initWithDouble:-97.74306080];
    return [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
}

@end