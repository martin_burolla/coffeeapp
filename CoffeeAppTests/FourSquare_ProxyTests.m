//
//  CoffeeAppTests.m
//  CoffeeAppTests
//
//  Created by Marty Burolla on 11/12/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FourSquareProxy.h"
#import "UnitTestHelper.h"
#import "CoffeeHouseMoreInfoViewModel.h"


@interface FourSquare_ProxyTests : XCTestCase

@property(nonatomic, strong) FourSquareProxy *fourSquareProxy;
@property(nonatomic, strong) XCTestExpectation *expectation;
@property(nonatomic, strong) NSArray<MKPointAnnotation *> *coffeeHouses;
@property(nonatomic, strong) CoffeeHouseMoreInfoViewModel *moreInfoViewModel;

@end

@implementation FourSquare_ProxyTests

- (void)setUp {
    [super setUp];
    _fourSquareProxy = [[FourSquareProxy alloc]init];
    _expectation =  [self expectationWithDescription:@"FourSquare API should return proper data."];
}

- (void)tearDown {
    _coffeeHouses = nil;
    _moreInfoViewModel = nil;
    [super tearDown];
}

- (void)test_GetCoffeeHouses {
    
    // Arrange.
    CLLocation *location =  [UnitTestHelper createLocationForDowntownAustin];
    
    // Act.
    [self.fourSquareProxy coffeeHousesNearLocation:location withCompletion:^(NSArray<MKPointAnnotation *> *coffeeHouses) {
        self.coffeeHouses = coffeeHouses;
        [self.expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        
        // Assert.
        XCTAssertTrue(self.coffeeHouses.count > 0, @"No coffee houses in austin?");
    }];
}

- (void)test_GetVenueDetails {
    
    // Act.
    [self.fourSquareProxy venueGetDetail:@"52869068498e3289da67410f" // Starbucks on Congress.
                          withCompletion:^(CoffeeHouseMoreInfoViewModel *coffeeHouseDetail) {
        self.moreInfoViewModel = coffeeHouseDetail;
        [self.expectation fulfill];
    }];
    
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        
        // Assert.
        XCTAssertTrue(self.moreInfoViewModel != nil, @"No details for Starbucks on Congress?");
    }];
}

@end