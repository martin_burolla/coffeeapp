//
//  CoffeeAppUITests.m
//  CoffeeAppUITests
//
//  Created by Marty Burolla on 11/12/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface CoffeeAppUITests : XCTestCase

@end

@implementation CoffeeAppUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here.
    [super tearDown];
}

- (void)testUI_HoundstoothCoffee {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    
    [NSThread sleepForTimeInterval:5];
    
    XCUIElement *element = [[[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] childrenMatchingType:XCUIElementTypeOther].element;
    
    [[[[[element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther] matchingIdentifier:@"Houndstooth Coffee, (512) 531-9417"] elementBoundByIndex:0] tap];
    
    [[[[[[[[[[[element childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeButton].element tap];
    
    [app.buttons[@"OK"] tap];
}

@end
